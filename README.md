SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

I. Kienle<br>
Grundlagenuntersuchungen zum saugseitigen Filter im DNOX-Fördermodul
<br>
Master's Thesis, 2020<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir kienle2020a && cd kienle2020a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/kienle2020a.git
```

After that, execute the file [installKienle2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/kienle2020a/-/blob/master/installKienle2020a.sh).

```
./installKienle2020a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installKienle2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/kienle2020a/-/blob/master/installKienle2020a.sh).
