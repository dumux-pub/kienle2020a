// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The two-phase porousmediumflow through filtermedium
 */

#ifndef DUMUX_EX_BASIC_PROBLEM_2P_HH
#define DUMUX_EX_BASIC_PROBLEM_2P_HH

#include <dune/grid/yaspgrid.hh>

//#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/h2on2.hh>

#include "spatialparams.hh"

namespace Dumux {

// forward declare problem
template <class TypeTag>
class Injection2PProblem;

namespace Properties {
// define the TypeTag for this problem with a cell-centered two-point flux approximation spatial discretization.
// Create new type tags
namespace TTag {
struct Injection2p { using InheritsFrom = std::tuple<TwoP>; };
//struct Injection2pCC { using InheritsFrom = std::tuple<Injection2p, CCTpfaModel>; };
struct Injection2pBox { using InheritsFrom = std::tuple<Injection2p, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Injection2p> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Injection2p> { using type = Injection2PProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Injection2p>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = InjectionSpatialParams<FVGridGeometry, Scalar>;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Injection2p> { using type = FluidSystems::H2ON2<GetPropType<TypeTag, Properties::Scalar>, FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/ true>>; };
} // end namespace Properties


/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief The two-phase porousmediumflow through filtermedium
 *
 * The domain is sized 0.006 cm times 6.9 cm. 
 * In the middle of the domain is the filtermedium, a layer with a lower porosity and permeabiliy.
 * Water is injected at the left boundary of the domain from 0 cm to 0.5 cm.
 * A Dirichlet condition is used at this part. 
 * There is a solution dependent Neumann condition on the top of the domain.
 * The remaining boundaries are Neumann no-flow boundaries.
 *
 * This problem uses the \ref TwoPModel model.
 */
template<class TypeTag>
class Injection2PProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    enum { dimWorld = GridView::dimensionworld };
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    enum
    {
        contiH2OEqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
        contiN2EqIdx = Indices::conti0EqIdx + FluidSystem::N2Idx,
    };
    
    // phase indices
    enum
    {
	wPhaseIdx = FluidSystem::liquidPhaseIdx,
	nPhaseIdx = FluidSystem::gasPhaseIdx
    };
    
    public:
    Injection2PProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        nTemperature_       = getParam<int>("Problem.NTemperature");
        nPressure_          = getParam<int>("Problem.NPressure");
        pressureLow_        = getParam<Scalar>("Problem.PressureLow");
        pressureHigh_       = getParam<Scalar>("Problem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("Problem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("Problem.TemperatureHigh");
        temperature_        = getParam<Scalar>("Problem.InitialTemperature");
        depthBOR_           = getParam<Scalar>("Problem.DepthBOR");
        name_               = getParam<std::string>("Problem.Name");
        filterthickness_    = getParam<Scalar>("Problem.filterthickness");

        // initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
   }
	  
	
    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    std::string name() const
    { return name_+"-2p"; }

    /*!
     * \brief Returns the temperature \f$ K \f$
     */
    Scalar temperature() const
    {
        return temperature_; // [K]
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
       BoundaryTypes bcTypes;
	if (globalPos[0] < 0 + eps_&& globalPos[1] < 0.005 + eps_)
            bcTypes.setAllDirichlet();  
        else
        bcTypes.setAllNeumann();
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
      
      PrimaryVariables values(0.0);
      values[Indices::pressureIdx] = 1e5 - (1000 * 9.81 * globalPos[1]);
      values[Indices::saturationIdx] = 0.0;
     
      return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     */
   NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        // initialize values to zero, i.e. no-flow Neumann boundary conditions
        NumEqVector values(0.0);
	const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        const auto& globalPos = scvf.ipGlobal();

        if (globalPos[1] > 0.0692 - eps_ && globalPos[0] > 0.000261 + filterthickness_)
	{
	    const Scalar elemPressW = volVars.pressure(wPhaseIdx);	//Pressures
	    const Scalar elemPressN = volVars.pressure(nPhaseIdx);
	    
	    const Scalar densityW = volVars.fluidState().density(wPhaseIdx);  //Densities
            const Scalar densityN = volVars.fluidState().density(nPhaseIdx);

            const Scalar elemMobW = volVars.mobility(wPhaseIdx);      //Mobilities
            const Scalar elemMobN = volVars.mobility(nPhaseIdx);
	    
 	    const Scalar pOut = 99155;
	    const Scalar coarseK = 1e-9;
	    
	    Scalar qW = densityW * elemMobW * coarseK * (elemPressW-pOut);   //[kg/ m s]
	    Scalar qN = densityN * elemMobN * coarseK * (elemPressN-pOut);   //[kg/ m s]
	    //std::cout << "flux Water before sign change " << qW << std::endl;
	    //std::cout << " globalPos " << globalPos[0] << " " << globalPos[1] << std::endl;
	    
	    
	    if (qW < 0.0)
	      qW = 0.0;
	    if (qN < 0.0)
	      qN = 0.0;
	    
	    values[contiH2OEqIdx] = qW;
            values[contiN2EqIdx] = qN;
	}
	return values;
    }

    // \}


    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param globalPos The position for which the source term should be evaluated
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

	//pWSn
	  if (globalPos[0] > 0.000261 && globalPos[0] < 0.000261 + filterthickness_){
	    values[Indices::pressureIdx] = 1e5;
	    values[Indices::saturationIdx] = 0.2;  }
	  else{
	    values[Indices::pressureIdx] = 1e5;
	    values[Indices::saturationIdx] = 1; }

        return values;
    }

    // \}

    //! set the time for the time dependent boundary conditions (called from main)
    void setTime(Scalar time)
    { time_ = time; }

private:
    static constexpr Scalar eps_ = 1e-6;
    std::string name_; //! Problem name
    Scalar aquiferDepth_; //! Depth of the aquifer in m
    Scalar injectionDuration_; //! Duration of the injection in seconds
    Scalar time_;
    Scalar temperature_;
    Scalar depthBOR_;
    Scalar filterthickness_ ;
    Scalar coarseK ;
    
    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
};

} //end namespace Dumux

#endif
